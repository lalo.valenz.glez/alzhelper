app.controller('LoginController', function ($scope, $state, Network, User, $cordovaFileTransfer, $cordovaFile, $rootScope) {
    $scope.user = {};

    $scope.goTologin =  function(){
    	if($scope.user.username != ''){
    		if($scope.user.password != ''){
    			console.log($scope.user);
    			Network.signIn($scope.user).then(function(resp){
    				console.log(resp.data);
                    if(resp.data.user.status == 2){
                        var url = 'https://api.qrserver.com/v1/create-qr-code/?size=150x150&data='+resp.data.user.id;
                        var targetPath = cordova.file.dataDirectory + "qr_"+resp.data.user.id+".jpg";
                        $rootScope.qrPath = targetPath;
                        var trustHosts = true;
                        var options = {};
                        $cordovaFileTransfer.download(url, targetPath, trustHosts, options).then(function(resp){
                            console.log(JSON.stringify(resp));
                        }, function (err) {
                            console.log(JSON.stringify(err));
                        }, function(progress){
                            console.log(JSON.stringify(progress));
                        });
                        swal("Yeah","Welcome", "success");
                        User.setUserParams(resp.data.user);
                        $state.go('app.dash',{id: resp.data.user.status});
                    }
                    swal("Yeah","Welcome", "success");
    				User.setUserParams(resp.data.user);
                    $state.go('app.dash',{id: resp.data.user.status});
    			}, function(err){
    				console.log(err);
    			});
    		}else{
    			swal("Ups","Password is empty", "warning");
    		}
    	}else{
    		swal("Ups","Username is empty", "warning");
    	}
    	
    }

    $scope.goToRegister = function(){
        
    }

});