app.controller('DashController', function ($scope, $stateParams, $state, Network, User, $cordovaFile, $rootScope) {
  $scope.user = User.getCurrentUser();
  if ($stateParams.id == 1) {
    $scope.$on('$ionicView.beforeEnter', function () {
      $scope.user = User.getCurrentUser();
      var id = $scope.user.userID;
      Network.getPatientsLocations(id).then(function (resp) {
        console.log(resp.data.data);
        $scope.patiensts = resp.data.data.rows;
      }, function (err) {
        console.log(err);
      });
    });
  } else {
    $scope.$on('$ionicView.beforeEnter', function () {
      $scope.user = User.getCurrentUser();
      $cordovaFile.checkFile(cordova.file.dataDirectory, "qr_" + $scope.user.id + ".jpg")
        .then(function (success) {
          console.log(JSON.stringify(success));
        }, function (error) {
          console.log(JSON.stringify(error));
        });
    });
  }

});
