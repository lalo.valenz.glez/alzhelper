﻿app.controller('AppCtrl', function ($scope, $ionicModal, $ionicPopover, $timeout, User, Network, $state, $cordovaBarcodeScanner) {
  // Form data for the login modal
  $scope.loginData = {};

  $scope.user = User.getCurrentUser();

  $scope.sing_out = function () {
    //Network.signOut().then(function(resp){
    User.logOut();
    $state.go('login');
    //}, function(err){
    // console.log(err);
    //});

  }
  var navIcons = document.getElementsByClassName('ion-navicon');
  for (var i = 0; i < navIcons.length; i++) {
    navIcons.addEventListener('click', function () {
      this.classList.toggle('active');
    });
  }

  var fab = document.getElementById('fab');
  fab.addEventListener('click', function () {
    $cordovaBarcodeScanner.scan().then(function (data) {
      if (!data.cancelled) {
        Network.setPatient({patient_id: data.text, keeper_id: User.getCurrentUser().userID}).then(function (res) {
          console.log(JSON.stringify(res));
        }).catch(function (e) {
          console.log(JSON.stringify(e));
        });
      }
    }).catch(function (e) {
      console.log(JSON.stringify(e));
    });
  });

  // .fromTemplate() method
  var template = `<ion-popover-view>
       <ion-header-bar>
           <h1 class="title">My Popover Title</h1>
       </ion-header-bar>
       <ion-content class="padding">
           My Popover Contents
       </ion-content>
    </ion-popover-view>`;

  $scope.popover = $ionicPopover.fromTemplate(template, {
    scope: $scope
  });
  $scope.closePopover = function () {
    $scope.popover.hide();
  };
  //Cleanup the popover when we're done with it!
  $scope.$on('$destroy', function () {
    $scope.popover.remove();
  });
});
