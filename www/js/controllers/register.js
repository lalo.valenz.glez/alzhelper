app.controller('RegisterController', function ($scope, $state, Network, User) {
    $scope.user = {};

    $scope.sing_up = function(){
        if($scope.password == $scope.rpass){
            Network.createNewUser($scope.user).then(function(resp){
                console.log(resp);
                if(resp.data.success){
                    $scope.id = resp.data.data.id;
                    swal({
                          title: "Role Selection",
                          text: "What type of user are you?",
                          type: "warning",
                          showCancelButton: true,
                          confirmButtonColor: "#3498db",
                          confirmButtonText: "Patient",
                          cancelButtonColor: "#2ecc71",
                          cancelButtonText: "Keeper",
                          closeOnConfirm: true,
                          closeOnCancel: true,
                    },
                    function(isConfirm){
                        var role = 0;
                        if(isConfirm){
                            role = 1;
                            Network.SelectRole($scope.id, role).then(function(resp){
                                console.log(resp);
                                swal("Yeah!", "Registration has been successful", "success");
                            },function(err){
                                console.log(err);
                            });
                        }else{
                            role = 2;
                            Network.SelectRole($scope.id, role).then(function(resp){
                                console.log(resp);
                                swal("Yeah!", "Registration has been successful.", "success");
                            },function(err){
                                console.log(err);
                            });
                        }
                    });
                }else{
                    swal("Ups!", "something happens ):", "warning");
                }
            }, function(err){

            });
        }
    };

});