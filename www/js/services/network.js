
let HOSTNAME = 'http://10.6.0.77:3000/'
angular.module('starter')
  .factory('Network', function NetworkFactory($http) {
    return {
      signIn: function (user) {
        return $http({
          method: 'POST',
          url: HOSTNAME + 'users/login/',
          data: {
              username: user.username,
              password: user.password
          },
          headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json'
          },
          timeout: 10000
        });
      },
      createNewUser: function (user) {
        return $http({
          method: 'POST',
          url: HOSTNAME + 'users/create',
          data: {
              name: user.name,
              last_name: user.last_name,
              username: user.username,
              email: user.email,
              password: user.password
          },
          headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json'
          },
          timeout: 10000
        });
      },
      setPatient: function (user) {
        return $http({
          method: 'POST',
          url: HOSTNAME + 'users/set_patient',
          data: {
            keeper_id: user.keeper_id,
            patient_id: user.patient_id
          },
          headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json'
          },
          timeout: 10000
        });
      },
      SelectRole: function (id, role) {
        return $http({
          method: 'PUT',
          url: HOSTNAME + 'users/status',
          data: {
              id: id,
              status: role,
          },
          headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json'
          },
          timeout: 10000
        });
      },
      getPatientsLocations: function (id) {
        return $http({
          method: 'GET',
          url: HOSTNAME + 'patients/'+id,
          data: {},
          headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json'
          },
          timeout: 10000
        });
      },
      signOut: function () {
        return $http({
          method: 'DELETE',
          url: HOSTNAME + 'users/sing_out/',
          data: {},
          headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json'
          },
          timeout: 10000
        });
      },
     }
  });
