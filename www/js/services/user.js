angular.module('starter')

.factory('User', function() {
    var user = {};
    return {
        init: function() {
            if (window.localStorage['userQAQC'])
                user = JSON.parse(window.localStorage['userQAQC']);
        },
        setUserParams: function(userParams) {
            user = {};
            user.userID = userParams.id;
            user.name = userParams.name;
            user.last_name = userParams.last_name;
            user.email = userParams.email;
            user.username = userParams.username;
            user.password = userParams.password;
            user.status = userParams.enterprise;
            window.localStorage['userQAQC'] = JSON.stringify(user);
        },
        getCurrentUser: function(){
            return user;
        },
        getUser: function() {
            if (user)
                return {
                    username: user.username,
                    password: user.password
                };
            else
                return {};
        },
        logOut: function() {
            delete user;
            window.localStorage['userQAQC'] = JSON.stringify(user);
        }
    }
});
